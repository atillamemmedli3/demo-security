package az.ingress.demosecurity;

import az.ingress.demosecurity.config.jwt.JwtService;
import az.ingress.demosecurity.model.Authority;
import az.ingress.demosecurity.model.Role;
import az.ingress.demosecurity.model.User;
import az.ingress.demosecurity.repository.UserRepository;
import java.time.Duration;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class DemoSecurityApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder encoder;
    private final JwtService jwtService;

    public static void main(String[] args) {
        SpringApplication.run(DemoSecurityApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Trying to create user");
        Authority authority = new Authority();
        authority.setRole(Role.ROLE_ADMIN);
        User taleh = User.builder()
                .username("taleh")
                .password(encoder.encode("102030"))
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .credentialsNonExpired(true)
                .isEnabled(true)
                .authorities(List.of(authority))
                .build();

        userRepository.save(taleh);


        String jwt = jwtService.issueToken(taleh, Duration.ofHours(1));
        log.info("JWT for user is {}" ,jwt );

        log.info("Parse JWT {}" , jwtService.parseToken(jwt));

    }
}
